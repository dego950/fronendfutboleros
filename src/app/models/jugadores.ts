export  class Jugadores {
  constructor(
    public id: number,
    public codigo: string,
    public nombres: string,
    public apellidos: string,
    public cedula: string,
    public expedicion: any,
    public nacimineto: any,
    public rh: string,
    public foto: string,
    public createdAt: any,
    public updatedAt: any
  ) {
  }
}
