import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.css']
})
export class DefaultComponent implements OnInit {
  public title: string;

  constructor() {
    this.title = 'home';
  }

  ngOnInit() {
    console.log('Default.component cargado')
  }

}
