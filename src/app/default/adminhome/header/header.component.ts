import { Component, OnInit, DoCheck } from '@angular/core';
import {UserService} from '../../../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, DoCheck {
  public indentity;
  public token;

  public title: string;

  constructor(
    private  userService: UserService,
    private router: Router,
    private activeRoute: ActivatedRoute
  ) {
    this.title = 'Admin home';
    this.indentity = this.userService.getIdentity();
    this.token = this.userService.getToken();
  }

  ngOnInit() {
    this.logout();
  }

  ngDoCheck() {
    this.indentity = this.userService.getIdentity();
    this.token = this.userService.getToken();
  }

  logout(){
    this.activeRoute.params.subscribe(params =>{
      let logout = +params['sure'];
      if (logout == 1){
        localStorage.removeItem('identity');
        localStorage.removeItem('token');

        this.indentity = null;
        this.token = null;

        //redireccion
        this.router.navigate(['home']);
      }
    });
  }

}
