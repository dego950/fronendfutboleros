import { Component, OnInit, DoCheck} from '@angular/core';
import {UserService} from '../../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-adminhome',
  templateUrl: './adminhome.component.html',
  styleUrls: ['./adminhome.component.css'],
  providers: [UserService]
})
export class AdminhomeComponent implements OnInit, DoCheck {
  public indentity;
  public token;
  public title: string;
  private showElement: boolean;

  constructor(
    private  userService: UserService,
    private router: Router,
    private activeRoute: ActivatedRoute


  ) {
    this.title = 'Admin home';
    this.indentity = this.userService.getIdentity();
    this.token = this.userService.getToken();
  }

  ngOnInit() {
    this.logout();
    this.myFunctionRemovelocalstorage();
  }

  ngDoCheck() {
    this.indentity = this.userService.getIdentity();
    this.token = this.userService.getToken();

  }

  logout(){
    this.activeRoute.params.subscribe(params =>{
      let logout = +params['sure'];
      if (logout == 1){
        localStorage.removeItem('identity');
        localStorage.removeItem('token');

        this.indentity = null;
        this.token = null;

        //redireccion
        this.router.navigate(['home']);
      }
    });
  }

  myFunctionRemovelocalstorage() {
    this.showElement = true;
    setTimeout(function() {
      console.log(this.showElement); // Will result in undefined;
      localStorage.removeItem('identity');
      localStorage.removeItem('token');
      this.showElement = false; // Here, this, reference to context of the function wrapper
    }, 6000*60);
  }
}
