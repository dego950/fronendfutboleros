import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService]
})
export class LoginComponent implements OnInit, OnDestroy {
  public  tittle: string;
  public user: User;
  public token;
  public identify;
  public status: string;


  constructor(
    private userService: UserService,
    private router: Router,
    private activeRoute: ActivatedRoute
  ) {
    this.tittle = 'Identificate';
    this.user = new User(1, 'Jugador', '', '', '', '');
  }

  ngOnInit() {
    console.log('login.component cargado');
  }

  onSubmit(form) {
    //console.log(this.user);
    this.userService.signup(this.user).subscribe(
      response => {
        //token
        if (response.status != 'error'){
          this.status = 'success'
          this.token = response;
          localStorage.setItem('token', this.token);

          //objeto de usuario identificado
          this.userService.signup(this.user, true).subscribe(
            response => {
              this.identify = response;
              localStorage.setItem('identity', JSON.stringify(this.identify));
              //Redireccion
              this.router.navigate(['/adminhome/dashboard']);
            },
            error => {
              console.log( error as any);
            }
          );
        }else {
          this.status = 'error';
        }
      },
      error => {
        console.log( error as any);
      }
    );
  }

  ngOnDestroy(): void {
    location.reload()
  }

}
