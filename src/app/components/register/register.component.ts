import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { UserService} from '../../services/user.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [UserService]
})
export class RegisterComponent implements OnInit {
  public title: string;
  public user: User;
  public status: string;

  constructor(private userService: UserService) {
    this.title = 'registrate';
    this.user = new User(1, 'Admin', '', '', '', '');
  }

  ngOnInit() {
    console.log('register.controller cargado');
  }

  onSubmit() {
     console.log(this.user);
     this.userService.register(this.user).subscribe(
      response => {
        if (response == 'success') {
          this.status = response.status;
          // Vaciar el formulario
          this.user = new User(1, 'Jugador', '', '', '', '');
        } else {
          this.status = 'error';
        }
      },
      error => {
        console.log(error as any);
      }
    );
  }
}
