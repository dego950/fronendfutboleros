import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {DefaultComponent} from './default/default/default.component';
import {AdminhomeComponent} from './default/adminhome/adminhome.component';
import {DashboardComponent} from './default/adminhome/dashboard/dashboard.component';
import {NotpageComponent} from './default/adminhome/notpage/notpage.component';


const routes: Routes = [
  {path: '', component: DefaultComponent},
  {path: 'home', component: DefaultComponent},
  {path: 'login', component: LoginComponent},
  {path: 'logout/:sure', component: LoginComponent},
  {path: 'adminhome', component: AdminhomeComponent},
  {path: 'adminhome/logout/:sure', component: AdminhomeComponent},
  {path: 'adminhome/dashboard', component: DashboardComponent},
  {path: 'adminhome/registro', component: RegisterComponent},
  {path: 'adminhome/notpage', component: NotpageComponent},

  {path: '**', component: DefaultComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
