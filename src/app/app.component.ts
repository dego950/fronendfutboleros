import { Component, OnInit, DoCheck } from '@angular/core';
import { UserService} from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService]
})
export class AppComponent implements OnInit, DoCheck{
  title = 'FronendFutboleros';
  public indentity;
  public token;

  constructor(private  userService: UserService) {
    this.indentity = this.userService.getIdentity();
    this.token = this.userService.getToken();
  }

  ngOnInit() {
    console.log('app.component cargado');
  }

  ngDoCheck(){
    this.indentity = this.userService.getIdentity();
    this.token = this.userService.getToken();
  }
  refresh(): void {
    window.location.reload();
  }
}
