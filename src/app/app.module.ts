import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { DefaultComponent } from './default/default/default.component';
import { AdminhomeComponent } from './default/adminhome/adminhome.component';
import { HeaderComponent } from './default/adminhome/header/header.component';
import { LeftpanelComponent } from './default/adminhome/leftpanel/leftpanel.component';
import { DashboardComponent } from './default/adminhome/dashboard/dashboard.component';
import { NotpageComponent } from './default/adminhome/notpage/notpage.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    DefaultComponent,
    AdminhomeComponent,
    HeaderComponent,
    LeftpanelComponent,
    DashboardComponent,
    NotpageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
